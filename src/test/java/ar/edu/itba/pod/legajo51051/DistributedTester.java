package ar.edu.itba.pod.legajo51051;

import java.rmi.RemoteException;
import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.SPNode;
import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.api.SignalProcessor;
import ar.edu.itba.pod.impl.StandaloneSignalProcessor;
import ar.edu.itba.pod.legajo51051.impl.MultiThreadedSignalProcessor;
import ar.edu.itba.pod.signal.source.RandomSource;
import ar.edu.itba.pod.signal.source.Source;

import com.google.common.collect.Lists;

public class DistributedTester {

	private static int NODES_QTY = 2;
	private static int INITIAL_SIG_QTY = 5000;

	private static List<SPNode> nodes;
	private static List<SignalProcessor> processors;
	private static Source src;
	private static SignalProcessor reference;

	@BeforeClass
	public static void initProcessors() throws Exception {
		reference = new StandaloneSignalProcessor();
		src = new RandomSource();
		nodes = Lists.newArrayList();
		processors = Lists.newArrayList();
		for(int i = 0; i < NODES_QTY; i++) {
			MultiThreadedSignalProcessor p = new MultiThreadedSignalProcessor(Runtime.getRuntime().availableProcessors());
			nodes.add(p);
			processors.add(p);
		}
	}

	@Test
	public void joinOneToCluster() {
		try {
			nodes.get(0).join("cluster");
		} catch (RemoteException e) {
			Assert.fail();
		}
	}

	@Test
	public void addSignalsToFirstNode() {
		try {
			for (int i = 0; i < INITIAL_SIG_QTY; i++) {
				Signal s = src.next();
				reference.add(s);
				processors.get(0).add(s);
			}
		} catch (RemoteException e) {
			Assert.fail();
		}
	}
	
	@Test
	public void resultEqualsOnlyOneNodeInCluster() {
		assertResultIsTheSame(processors.get(0), reference);
	}

	@Test
	public void joinAllToCluster() {
		try {
			nodes.get(0).getStats().print(System.out);
			for (int i = 1; i < NODES_QTY; i++) {
				nodes.get(i).join("cluster");
				//give time to nodes to distribute signals
				Thread.sleep(10);
				nodes.get(i).getStats().print(System.out);
			}
		} catch (RemoteException | InterruptedException e) {
			Assert.fail();
		}
	}
	
	@Test
	public void resultEqualsAllNodesInCluster() {
		assertResultIsTheSame(processors.get(0), reference);
	}
	
	private void assertResultIsTheSame(SignalProcessor toTest, SignalProcessor reference) {
		Signal s = src.next();
		try {
			Result testResult = toTest.findSimilarTo(s);
			Result referenceResult = reference.findSimilarTo(s);
			Assert.assertEquals(referenceResult, testResult);
		} catch (RemoteException e) {
			Assert.fail();
		}
	}
}
