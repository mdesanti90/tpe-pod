package ar.edu.itba.pod.legajo51051;

import ar.edu.itba.pod.api.SPNode;
import ar.edu.itba.pod.api.SignalProcessor;
import ar.edu.itba.pod.impl.SideBySideTester;
import ar.edu.itba.pod.legajo51051.impl.MultiThreadedSignalProcessor;

public class SideBySideNoRMITest extends SideBySideTester {

	private static SignalProcessor sp = null;

	@Override
	protected SignalProcessor init() throws Exception {
		sp = new MultiThreadedSignalProcessor(Runtime.getRuntime().availableProcessors());
		((SPNode)sp).exit();
		return sp;
	}

}
