package ar.edu.itba.pod.legajo51051.impl;

import java.io.Serializable;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;

public class AddressSignal implements Serializable {

	private Address addr;
	private Signal signal;
	
	public AddressSignal(Address addr, Signal signal) {
		this.addr = addr;
		this.signal = signal;
		
	}
	
	
	public Address getAddr() {
		return addr;
	}
	
	public Signal getSignal() {
		return signal;
	}
	
}
