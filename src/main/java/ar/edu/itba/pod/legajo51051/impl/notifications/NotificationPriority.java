package ar.edu.itba.pod.legajo51051.impl.notifications;

public enum NotificationPriority {

	URGENT(1), NORMAL(2);
	
	private int priority;
	
	private NotificationPriority(int priority) {
		this.priority = priority;
	}
	
	public int getPriority() {
		return priority;
	}
	
}
