package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;

public class AddSignalRequest implements Serializable, Request {

	private static final long serialVersionUID = 1L;
	private final boolean isBackup;
	private final Signal signal;
	private final Address backupHolder;
	public AddSignalRequest(Address trueSignalCarrier, Signal signal, boolean isBackup) {
		this.isBackup = isBackup;
		this.signal = signal;
		this.backupHolder = trueSignalCarrier;
	}
	
	public Signal getSignal() {
		return signal;
	}
	
	public boolean isBackup() {
		return isBackup;
	}
	
	public Address getBackupHolder() {
		return backupHolder;
	}
	
}
