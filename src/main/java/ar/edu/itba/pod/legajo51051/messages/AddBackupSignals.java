package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;
import java.util.Collection;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;

public class AddBackupSignals implements Serializable {
	
	private Collection<Signal> signals;
	private Address holder;
	
	public AddBackupSignals(Collection<Signal> signals, Address addr) {
		this.signals = signals;
		this.holder = addr;
	}
	
	public Address getHolder() {
		return holder;
	}
	
	public Collection<Signal> getSignals() {
		return signals;
	}

}
