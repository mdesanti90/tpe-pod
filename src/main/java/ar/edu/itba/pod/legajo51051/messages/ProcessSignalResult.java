package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

import ar.edu.itba.pod.api.Result;

public class ProcessSignalResult implements Serializable {
	
	private final Result result;
	private final long processId;
	private final long timeStamp;
	
	public ProcessSignalResult(long timeStamp, long processId, Result result) {
		this.result = result;
		this.processId = processId;
		this.timeStamp = timeStamp;
	}
	
	public Result getResult() {
		return result;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}

}
