package ar.edu.itba.pod.legajo51051.impl;

import java.rmi.RemoteException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.legajo51051.impl.notifications.AddSignalNotification;
import ar.edu.itba.pod.legajo51051.impl.notifications.NewMemberNotification;
import ar.edu.itba.pod.legajo51051.impl.notifications.NotificationPriority;
import ar.edu.itba.pod.legajo51051.impl.notifications.PriorityNotification;
import ar.edu.itba.pod.legajo51051.impl.notifications.ProcessSignalRequestNotification;
import ar.edu.itba.pod.legajo51051.impl.notifications.SystemRecoveryAddSignalNotification;
import ar.edu.itba.pod.legajo51051.messages.AddBackupSignals;
import ar.edu.itba.pod.legajo51051.messages.AddSignalAck;
import ar.edu.itba.pod.legajo51051.messages.AddSignalRequest;
import ar.edu.itba.pod.legajo51051.messages.CleanExit;
import ar.edu.itba.pod.legajo51051.messages.InitialSyncDone;
import ar.edu.itba.pod.legajo51051.messages.LoadBalancingMessage;
import ar.edu.itba.pod.legajo51051.messages.OwnerChangedWarning;
import ar.edu.itba.pod.legajo51051.messages.ProcessSignalRequest;
import ar.edu.itba.pod.legajo51051.messages.ProcessSignalResult;
import ar.edu.itba.pod.legajo51051.messages.ReadyToPlay;
import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryAddBackupSignalACK;
import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryAddSignalACK;
import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryAddSignalRequest;
import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryDone;
import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryStart;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class ExternalRelations extends ReceiverAdapter implements Runnable {

	private final PriorityBlockingQueue<PriorityNotification> notifications;
	private final BlockingQueue<Address> members;
	private final JChannel channel;
	private final Random random;
	private final AtomicBoolean degraded;
	private final MultiThreadedSignalProcessor processor;
	private final ConcurrentHashMap<Long, PendingProcess> pendingResults = new ConcurrentHashMap<>();
	private final Logger log;
	private final BlockingQueue<SystemRecoveryAddSignalNotification> recoveryRequests;
	private final AtomicLong id = new AtomicLong(0);
	private final Thread systemRecovery;
	private final BlockingQueue<Long> systemRecoverySentSignals;
	private final BlockingQueue<Long> systemRecoverySentBackupSignals;
	private final Semaphore systemRecoveryWaiter;
	private final Semaphore systemRecoveryStarterWaiter;
	private final Semaphore newNodeSync;
	private int size;

	public ExternalRelations(MultiThreadedSignalProcessor processor)
			throws Exception {
		Preconditions.checkNotNull(processor);
		channel = new JChannel();
		random = new Random();
		notifications = new PriorityBlockingQueue<>(10,
				new Comparator<PriorityNotification>() {
					@Override
					public int compare(PriorityNotification o1,
							PriorityNotification o2) {
						return o1.getPriority().getPriority()
								- o2.getPriority().getPriority();
					}
				});
		members = new LinkedBlockingQueue<>();
		degraded = new AtomicBoolean(false);
		this.processor = processor;
		this.log = Logger.getLogger("ExternalRelations");
		this.systemRecoverySentSignals = new LinkedBlockingQueue<>();
		this.systemRecoverySentBackupSignals = new LinkedBlockingQueue<>();
		this.recoveryRequests = new LinkedBlockingQueue<>();
		systemRecovery = new Thread(new SystemRecoveryTrhead());
		this.systemRecoveryWaiter = new Semaphore(0);
		this.systemRecoveryStarterWaiter = new Semaphore(0);
		this.newNodeSync = new Semaphore(0);
	}

	@Override
	public void run() {
		boolean interrupted = false;
		try {
			boolean b = degraded.compareAndSet(false, true);
			while (!newNodeSync.tryAcquire(size - 1, 1000,
					TimeUnit.MILLISECONDS)) {
				log.info("Waiting for other nodes to send information!"
						+ newNodeSync.availablePermits() + (size - 1));
			}
			degraded.set(!b);
			log.info("Ready to work!");
			while (!interrupted) {
				PriorityNotification notification = notifications.take();
				notification.process();
			}
		} catch (InterruptedException e) {
			interrupted = true;
		}
	}

	public boolean join(String clusterName) {
		try {
			channel.connect(clusterName);
			channel.setReceiver(this);
			channel.setDiscardOwnMessages(true);
			members.addAll(channel.getView().getMembers());
			size = members.size();
			log.info("Joined " + clusterName);
			systemRecovery.start();
			degraded.set(channel.getView().getMembers().size() == 1);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void newMembers(List<Address> newMembers) {
		if (newMembers.size() > members.size()) {
		} else if (newMembers.size() < members.size()) {
			for (Address addr : findNewMembers(new LinkedList<>(members),
					newMembers)) {
				notifications.add(new FallenNodeNotification(addr));
			}

		}
	}

	private List<Address> findNewMembers(List<Address> newMembers,
			List<Address> oldMembers) {
		List<Address> ret = Lists.newLinkedList();
		for (Address addr : newMembers) {
			if (!oldMembers.contains(addr)) {
				ret.add(addr);
			}
		}
		return ret;
	}

	public void add(Signal signal, boolean systemRecovery, long id)
			throws RemoteException {
		View view = channel.getView();
		if (view != null) {
			internalAdd(signal, systemRecovery, id);

		} else {
			processor.immediatelyAddSignal(signal);
			processor.sentSignal(signal);
		}
		return;
	}

	public void add(List<Signal> signal, boolean systemRecovery, long id)
			throws RemoteException {
		internalAdd(signal, systemRecovery, id);

		return;
	}

	private void internalAdd(Signal signal, boolean systemRecovery, long id) {
		int size = 1;
		int luckyMember = 0;
		int backupMember = 0;
		List<Address> members = channel.getView().getMembers();
		size = members.size();
		luckyMember = random.nextInt(size);
		backupMember = random.nextInt(size);
		while (size != 1 && luckyMember == backupMember) {
			luckyMember = random.nextInt(size);
			backupMember = random.nextInt(size);
		}
		Address backupAddr = members.get(backupMember);
		Address luckyAddr = members.get(luckyMember);
		sendSignalTo(backupAddr, luckyAddr, signal, false, systemRecovery, id);
		sendSignalTo(luckyAddr, backupAddr, signal, true, systemRecovery, id);
	}

	private void internalAdd(List<Signal> signals, boolean systemRecovery,
			long id) {
		int size = 1;
		int luckyMember = 0;
		int backupMember = 0;
		List<Address> members = channel.getView().getMembers();
		size = members.size();
		luckyMember = random.nextInt(size);
		backupMember = random.nextInt(size);
		while (size != 1 && luckyMember == backupMember) {
			luckyMember = random.nextInt(size);
			backupMember = random.nextInt(size);
		}
		Address backupAddr = members.get(backupMember);
		Address luckyAddr = members.get(luckyMember);
		sendSignalTo(backupAddr, luckyAddr, signals, false, systemRecovery, id);
		sendSignalTo(luckyAddr, backupAddr, signals, true, systemRecovery, id);
	}

	/*
	 * First parameter indicates the address that holds the backup or the
	 * original signal.
	 */
	public void sendSignalTo(Address addr, Address dest, Signal signal,
			boolean isBackup, boolean systemRecovery, long id) {
		if (isMyAddress(dest)) {
			if (!isBackup) {
				processor.immediatelyAddSignal(signal);
				processor.sentSignal(signal);
				if (systemRecovery) {
					if (!systemRecoverySentSignals.add((long) 0)) {
						System.out.println("no deberia pasar linea 167");
					}
				}
			} else {
				processor.immediatelyAddBackupSignal(addr, signal);
				if (systemRecovery) {
					if (!systemRecoverySentBackupSignals.add((long) 0)) {
						System.out.println("No deberia pasar linea 174");
					}
				}
			}
		} else {
			try {
				if (!systemRecovery) {
					channel.send(dest, new AddSignalRequest(addr, signal,
							isBackup));
				} else {
					System.out.println("No deberia llegar aca");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void sendSignalTo(Address addr, Address dest, List<Signal> signal,
			boolean isBackup, boolean systemRecovery, long id) {
		if (isMyAddress(dest)) {
			if (!isBackup) {
				processor.immediatelyAddSignal(signal);
				if (systemRecovery) {
					if (!systemRecoverySentSignals.add((long) 0)) {
						System.out.println("no deberia pasar linea 167");
					}
				}
			} else {
				processor.immediatelyAddBackupSignal(addr, signal);
				if (systemRecovery) {
					if (!systemRecoverySentBackupSignals.add((long) 0)) {
						System.out.println("No deberia pasar linea 174");
					}
				}
			}
		} else {
			try {
				if (!systemRecovery) {
					System.out.println("No deberia llegar aca");
				}
				channel.send(dest, new SystemRecoveryAddSignalRequest(addr,
						signal, isBackup, id));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void process(long requestId, Signal signal) {
		if (!channel.isConnected()) {
			return;
		}
		BlockingQueue<Address> members = new LinkedBlockingQueue<>(channel
				.getView().getMembers());
		long timeStamp = System.nanoTime();
		pendingResults.put(requestId, new PendingProcess(timeStamp, signal,
				requestId, members));
		try {
			channel.send(null, new ProcessSignalRequest(timeStamp, requestId,
					channel.getAddress(), signal));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ResultContainer get(long id) {
		PendingProcess p = pendingResults.get(id);
		if (p == null) {
			return new ResultContainer(new LinkedList<Result>(), false);
		}
		List<Result> results = p.get();
		pendingResults.remove(id);
		return new ResultContainer(results, p.isRetry());
	}

	public String getAddressAsString() {
		return channel.getAddressAsString();
	}

	@Override
	public void viewAccepted(View view) {
		newMembers(view.getMembers());
	}

	@Override
	public void receive(Message msg) {
		Object obj = msg.getObject();

		if (obj instanceof AddSignalRequest) {
			AddSignalRequest req = (AddSignalRequest) obj;
			notifications.add(new AddSignalNotification(channel, msg.getSrc(),
					processor, req.isBackup(), req.getSignal(), req
							.getBackupHolder()));

		} else if (obj instanceof ProcessSignalRequest) {
			// log.info("ProcessSignalRequest");
			ProcessSignalRequest req = (ProcessSignalRequest) obj;
			notifications.add(new ProcessSignalRequestNotification(req
					.getTimeStamp(), processor, notifications, req.getSignal(),
					channel, req.getProcessId(), msg.getSrc()));
		} else if (obj instanceof LoadBalancingMessage) {
			// log.info("LoadBalancingRequest");
			LoadBalancingMessage req = (LoadBalancingMessage) obj;
			processor.immediatelyAddSignal(req.getSent());

		} else if (obj instanceof OwnerChangedWarning) {
			// log.info("OwnerChangedWarning");
			OwnerChangedWarning warning = (OwnerChangedWarning) obj;
			if (!isMyAddress(warning.getNewOwner())
					&& !isMyAddress(warning.getOldOwner())) {
				processor.changeBackups(warning.getOldOwner(),
						warning.getNewOwner(), warning.getSent());
			}

		} else if (obj instanceof ProcessSignalResult) {
			// log.info("ProcessSignalResult");
			ProcessSignalResult result = (ProcessSignalResult) obj;
			PendingProcess pResult = pendingResults.get(result.getProcessId());
			if (pResult != null
					&& pResult.getTimeStamp() == result.getTimeStamp()) {
				pResult.newResult(msg.getSrc(),
						((ProcessSignalResult) obj).getResult());
			}
		} else if (obj instanceof AddBackupSignals) {
			// log.info("AddBackupSignals");
			AddBackupSignals req = (AddBackupSignals) obj;
			processor.immediatelyAddBackupSignal(req.getHolder(),
					req.getSignals());

		} else if (obj instanceof ReadyToPlay) {
			notifications.add(new NewMemberNotification(processor, channel,
					members, msg.getSrc(), degraded, newNodeSync));
		} else if (obj instanceof SystemRecoveryAddSignalRequest) {
			if (!recoveryRequests.add(new SystemRecoveryAddSignalNotification(
					(SystemRecoveryAddSignalRequest) obj, msg.getSrc()))) {
				System.out.println("no deberia pasar! da false!");
			}
		} else if (obj instanceof SystemRecoveryAddSignalACK) {
			SystemRecoveryAddSignalACK ack = (SystemRecoveryAddSignalACK) obj;
			if (!systemRecoverySentSignals.add((long) 0)) {
				System.out.println("no deberia pasar - signal ack ");
			}
		} else if (obj instanceof SystemRecoveryAddBackupSignalACK) {
			SystemRecoveryAddBackupSignalACK ack = (SystemRecoveryAddBackupSignalACK) obj;
			if (!systemRecoverySentBackupSignals.add((long) 0)) {
				System.out.println("No deberia pasar - backup ack");
			}
		} else if (obj instanceof SystemRecoveryDone) {
			systemRecoveryWaiter.release();
		} else if (obj instanceof SystemRecoveryStart) {
			// log.info("Released one in systemRecoveryStartWaiter");
			systemRecoveryStarterWaiter.release();
		} else if (obj instanceof AddSignalAck) {
			processor.sentSignal(((AddSignalAck) obj).getSignal());
		} else if (obj instanceof InitialSyncDone) {
			// log.info("Got it!");
			newNodeSync.release();
		} else {
			log.info("No es nada...");
		}
	}

	public boolean isMyAddress(Address addr) {
		return channel.getAddress().equals(addr);
	}

	protected void readyToPlay() {
		try {
			channel.send(null, new ReadyToPlay());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void removeMe() {
		members.remove(channel.getAddress());
	}

	public void readyToExit() {
		try {
			channel.disconnect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isDegraded() {
		return degraded.get();
	}

	public class FallenNodeNotification implements PriorityNotification {

		private final Address fallenNode;
		private final static int CHUNK_SIZE = 500;

		public FallenNodeNotification(final Address fallenNode) {
			this.fallenNode = fallenNode;
		}

		public Address getFallenNode() {
			return fallenNode;
		}

		@Override
		public NotificationPriority getPriority() {
			return NotificationPriority.URGENT;
		}

		@Override
		public void process() {
			degraded.set(true);
			log.info("Node " + getFallenNode() + " has fallen");
			members.remove(getFallenNode());
			try {
				BlockingQueue<Signal> toSend = new LinkedBlockingQueue<>(
						processor.getBackedUpSignals(getFallenNode()));
				toSend.addAll(processor.systemRecovery());
				channel.send(null, new SystemRecoveryStart());
				while (!systemRecoveryStarterWaiter.tryAcquire(
						members.size() - 1, 1000, TimeUnit.MILLISECONDS)) {
					log.info("waiting for all members to set for system recovery");
				}
				int chunks = toSend.size() / CHUNK_SIZE;
				for (int i = 0; i < chunks; i++) {
					List<Signal> chunk = Lists.newArrayList();
					toSend.drainTo(chunk, CHUNK_SIZE);
					long ident = id.incrementAndGet();
					// systemRecoverySentSignals.add(ident);
					// systemRecoverySentBackupSignals.add(ident);
					add(chunk, true, ident);
				}
				log.info("Done");
				int taken = 0;
				while (taken < chunks) {
					systemRecoverySentSignals.take();
					taken++;
				}
				taken = 0;
				while (taken < chunks) {
					systemRecoverySentBackupSignals.take();
					taken++;
				}
				// processor.removeBackupFor(getFallenNode());
				for (Address addr : members) {
					if (!channel.getAddress().equals(addr))
						channel.send(addr, new SystemRecoveryDone());
				}
				while (!systemRecoveryWaiter.tryAcquire(members.size() - 1,
						1000, TimeUnit.MILLISECONDS)) {
					log.info("waiting for all members "
							+ systemRecoveryWaiter.availablePermits()
							+ (members.size() - 1));
				}
			} catch (InterruptedException e) {
				return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (PendingProcess pending : pendingResults.values()) {
				if (pending.getMembers().contains(getFallenNode())) {
					pending.nodeHasFallen(Lists.newArrayList(members));
					try {
						long timeStamp = System.nanoTime();
						pending.setTimeStamp(timeStamp);
						channel.send(null, new ProcessSignalRequest(timeStamp,
								pending.getProcessId(), channel.getAddress(),
								pending.getBeingProcessed()));
					} catch (Exception e) {
					}
				}
			}
			log.info("System recovery complete");
			degraded.set(members.size() == 1);
		}

	}

	private class SystemRecoveryTrhead implements Runnable {
		@Override
		public void run() {
			boolean interrupted = false;
			while (!interrupted) {
				try {
					SystemRecoveryAddSignalNotification req = recoveryRequests
							.take();
					if (req.getRequest().isBackup()) {
						processor.immediatelyAddBackupSignal(req.getRequest()
								.getBackupHolder(), req.getRequest()
								.getSignal());
					} else {
						processor.immediatelyAddSignal(req.getRequest()
								.getSignal());
					}
					if (req.getRequest().isBackup()) {
						channel.send(req.getSender(),
								new SystemRecoveryAddBackupSignalACK(req
										.getRequest().getId()));
					} else {
						channel.send(req.getSender(),
								new SystemRecoveryAddSignalACK(req.getRequest()
										.getId()));
					}
				} catch (InterruptedException e) {
					interrupted = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
