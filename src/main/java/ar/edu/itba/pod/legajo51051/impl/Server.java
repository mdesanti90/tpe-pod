package ar.edu.itba.pod.legajo51051.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Server {

	private final int port;
	private final int threadNumber;
	private boolean interrupted = false;
	
	public Server(int port, int threadNumber) {
		this.port = port;
		this.threadNumber = threadNumber;
		BasicConfigurator.configure();
		 Logger.getRootLogger().setLevel(Level.OFF);
	}
	
	public void start() throws Exception {
		Registry reg;
		try {
			reg = LocateRegistry.createRegistry(port);
			System.out.println("Using " + threadNumber);
			MultiThreadedSignalProcessor signalProcessorImpl = new MultiThreadedSignalProcessor(threadNumber);
			Remote processor = UnicastRemoteObject.exportObject(signalProcessorImpl, 0);

			reg.bind("SignalProcessor", processor);
			reg.bind("SPNode", processor);
			System.out.println("Server started and listening on port " + port);
			System.out.println("Press <enter> to quit");
			new BufferedReader(new InputStreamReader(System.in)).readLine();
		} catch (RemoteException e) {
			System.out.println("Unable to start local server on port " + port);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Unexpected i/o problem");
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			System.out.println("Unable to register remote objects. Perhaps another instance is runnign on the same port?");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, Exception {
		if (args.length < 2) {
			System.out.println("Command line parameters: SampleServer <port> <threadNumber>");
			return;
		}
		new Server(Integer.parseInt(args[0]), Integer.parseInt(args[1])).start();
	}
	
	public void interrupt() {
		interrupted = true;
	}
	
	
	
}
