package ar.edu.itba.pod.legajo51051.impl;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.Signal;

public class SignalProcessorThread implements Callable<Result> {

	private final Signal toAnalyze;
	private final BlockingQueue<Signal> copy;

	public SignalProcessorThread(Signal toAnalyze, BlockingQueue<Signal> compareWith) {
		this.toAnalyze = toAnalyze;
		this.copy = compareWith;
	}

	@Override
	public Result call() {
		boolean finished = false;

		Result result = new Result(toAnalyze);

		while (!finished) {
			Signal mySignal = copy.poll();
			if (mySignal == null) {
				finished = true;
			} else {
				Result.Item item = new Result.Item(mySignal, toAnalyze.findDeviation(mySignal));
				result = result.include(item);
			}
		}
		return result;
	}

}
