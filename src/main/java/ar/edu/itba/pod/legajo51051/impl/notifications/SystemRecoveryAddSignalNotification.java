package ar.edu.itba.pod.legajo51051.impl.notifications;

import org.jgroups.Address;

import ar.edu.itba.pod.legajo51051.messages.SystemRecoveryAddSignalRequest;

public class SystemRecoveryAddSignalNotification {

	private final SystemRecoveryAddSignalRequest request;
	private final Address sender;
	
	public SystemRecoveryAddSignalNotification(SystemRecoveryAddSignalRequest request, Address sender) {
		this.sender = sender;
		this.request = request;
	}
	
	public SystemRecoveryAddSignalRequest getRequest() {
		return request;
	}
	
	public Address getSender() {
		return sender;
	}
	
}
