package ar.edu.itba.pod.legajo51051.impl.notifications;

import org.jgroups.Address;
import org.jgroups.JChannel;

import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.legajo51051.impl.MultiThreadedSignalProcessor;
import ar.edu.itba.pod.legajo51051.messages.AddSignalAck;

public class AddSignalNotification implements PriorityNotification {
	
	private final boolean isBackup;
	private final Signal signal;
	private final Address backupHolder;
	private MultiThreadedSignalProcessor processor;
	private final Address ackTo;
	private final JChannel channel;
	
	public AddSignalNotification(JChannel channel, Address ackTo, MultiThreadedSignalProcessor processor, boolean isBackup, Signal signal, Address backupHolder) {
		this.isBackup = isBackup;
		this.signal = signal;
		this.backupHolder = backupHolder;
		this.processor = processor;
		this.ackTo = ackTo;
		this.channel = channel;
	}
	
	@Override
	public void process() {
		if (isBackup) {
			processor.immediatelyAddBackupSignal(backupHolder, signal);
		} else {
			processor.immediatelyAddSignal(signal);
			try {
				channel.send(ackTo, new AddSignalAck(signal));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public NotificationPriority getPriority() {
		return NotificationPriority.NORMAL;
	}
	

}
