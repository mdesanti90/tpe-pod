package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

import org.jgroups.Address;

public class InitialSyncDone implements Serializable {

	private Address newMember;
	
	public InitialSyncDone(Address newMember) {
		this.newMember = newMember;
	}
	
	public Address getNewMember() {
		return newMember;
	}
	
}
