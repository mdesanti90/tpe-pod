package ar.edu.itba.pod.legajo51051.impl.notifications;

import java.util.concurrent.BlockingQueue;

import org.jgroups.Address;
import org.jgroups.JChannel;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.legajo51051.impl.MultiThreadedSignalProcessor;

public class ProcessSignalRequestNotification implements PriorityNotification {
	
	private MultiThreadedSignalProcessor processor;
	private BlockingQueue<PriorityNotification> notifications;
	private Signal analyze;
	private JChannel channel;
	private long processId;
	private Address src;
	private long timeStamp;
	
	public ProcessSignalRequestNotification(long timeStamp, MultiThreadedSignalProcessor processor, BlockingQueue<PriorityNotification> notifications, Signal analyze, JChannel channel, long processId, Address src) {
		this.processId = processId;
		this.processor = processor;
		this.notifications = notifications;
		this.analyze = analyze;
		this.channel = channel;
		this.src = src;
		this.timeStamp = timeStamp;
	}
	
	@Override
	public void process() {
		Result result = processor.processSignal(analyze);
		notifications.add(new ProcessResultNotification(timeStamp, channel, processId, src, result));
	}
	
	@Override
	public NotificationPriority getPriority() {
		return NotificationPriority.NORMAL;
	}

}
