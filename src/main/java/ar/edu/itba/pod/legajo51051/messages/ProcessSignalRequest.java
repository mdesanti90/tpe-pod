package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;

public class ProcessSignalRequest implements Serializable {

	private final Address sender;
	private final Signal signal;
	private final long processId;
	private final long timeStamp;
	
	public ProcessSignalRequest(long timeStamp, long processId, Address sender, Signal signal) {
		this.sender = sender;
		this.signal = signal;
		this.processId = processId;
		this.timeStamp = timeStamp;
	}
	
	public Address getSender() {
		return sender;
	}
	
	public Signal getSignal() {
		return signal;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
}
