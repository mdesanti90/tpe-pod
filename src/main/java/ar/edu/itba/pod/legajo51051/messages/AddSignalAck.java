package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

import ar.edu.itba.pod.api.Signal;

public class AddSignalAck implements Serializable {
	
	private Signal signal;
	
	public AddSignalAck(Signal signal) {
		this.signal = signal;
	}
	
	public Signal getSignal() {
		return signal;
	}

}
