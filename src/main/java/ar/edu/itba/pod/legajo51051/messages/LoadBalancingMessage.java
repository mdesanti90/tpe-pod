package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;
import java.util.List;

import ar.edu.itba.pod.api.Signal;

public class LoadBalancingMessage implements Serializable {
	
	private final List<Signal> sent;
	
	public LoadBalancingMessage(List<Signal> sent) {
		this.sent = sent;
	}
	
	public List<Signal> getSent() {
		return sent;
	}
}
