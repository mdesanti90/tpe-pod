package ar.edu.itba.pod.legajo51051.impl.notifications;

import org.apache.log4j.Logger;
import org.jgroups.Address;
import org.jgroups.JChannel;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.legajo51051.messages.ProcessSignalResult;

public class ProcessResultNotification implements PriorityNotification {
	
	private final Result result;
	private final Address replyTo;
	private final long processId;
	private final JChannel channel;
	private final Logger log = Logger.getLogger("Result");
	private final long timeStamp;
	
	public ProcessResultNotification(long timeStamp, final JChannel channel, long processId, final Address replyTo, final Result result) {
		this.result = result;
		this.replyTo = replyTo;
		this.processId = processId;
		this.channel = channel;
		this.timeStamp = timeStamp;
	}
	
	public Result getResult() {
		return result;
	}
	
	public Address getReplyTo() {
		return replyTo;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	@Override
	public NotificationPriority getPriority() {
		return NotificationPriority.NORMAL;
	}
	
	@Override
	public void process() {
		try {
			log.info("Result sent");
			channel.send(getReplyTo(), new ProcessSignalResult(timeStamp, getProcessId(), getResult()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
