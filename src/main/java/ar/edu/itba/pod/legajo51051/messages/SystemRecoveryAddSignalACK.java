package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

public class SystemRecoveryAddSignalACK implements Serializable {

	private long id;
	
	public SystemRecoveryAddSignalACK(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
}
