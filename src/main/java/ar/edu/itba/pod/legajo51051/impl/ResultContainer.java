package ar.edu.itba.pod.legajo51051.impl;

import java.util.List;

import com.google.common.collect.Lists;

import ar.edu.itba.pod.api.Result;

public class ResultContainer {

	private List<Result> results = Lists.newArrayList();
	private boolean retry =  false;
	
	public ResultContainer(List<Result> results, boolean retry) {
		this.results = results;
		this.retry = retry;
	}
	
	public List<Result> getResults() {
		return results;
	}
	
	public boolean getRetry() {
		return retry;
	}
}
