package ar.edu.itba.pod.legajo51051.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.jgroups.Address;
import org.jgroups.ReceiverAdapter;

import ar.edu.itba.pod.api.NodeStats;
import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.Result.Item;
import ar.edu.itba.pod.api.SPNode;
import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.api.SignalProcessor;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

public class MultiThreadedSignalProcessor extends ReceiverAdapter implements
		SignalProcessor, SPNode {

	private final BlockingQueue<Signal> mySignals;
	private final BlockingQueue<Signal> newSignals;
	private final Multimap<Address, Signal> backups;
	private ExecutorService mainThread;
	private final int nThreads;
	private final AtomicLong requestId = new AtomicLong(0);
	private final AtomicInteger requests = new AtomicInteger(0);
	private final AtomicBoolean acceptRequests = new AtomicBoolean(true);
	private AtomicBoolean started = new AtomicBoolean(false);

	private ExternalRelations externalRelations;
	private Thread externalRelationsThread;

	public MultiThreadedSignalProcessor(int nThreads) throws Exception {
		this.mySignals = new LinkedBlockingQueue<>();
		this.newSignals = new LinkedBlockingQueue<>();
		ArrayListMultimap<Address, Signal> list = ArrayListMultimap.create();
		this.backups = Multimaps.synchronizedListMultimap(list);
		this.mainThread = Executors.newFixedThreadPool(nThreads);
		this.nThreads = nThreads;
		externalRelations = new ExternalRelations(this);
		externalRelationsThread = new Thread(externalRelations);
	}

	@Override
	public void add(Signal signal) throws RemoteException {
		if (!acceptRequests.get()) {
			return;
		}
		int id = requests.incrementAndGet();
		synchronized (newSignals) {
			newSignals.add(signal);
		}
		externalRelations.add(signal, false, 0);
	}

	protected void sentSignal(Signal signal) {
		synchronized (newSignals) {
			newSignals.remove(signal);
		}

	}

	public boolean immediatelyAddSignal(Signal signal) {
		synchronized (mySignals) {
			return mySignals.add(signal);
		}
	}

	protected boolean immediatelyAddSignal(List<Signal> signals) {
		for (Signal signal : signals) {
			if (!immediatelyAddSignal(signal)) {
				return false;
			}
		}
		return true;
	}

	protected List<Signal> systemRecovery() {
		List<Signal> signals = new ArrayList<>(mySignals);
		backups.clear();
		mySignals.clear();
		return signals;
	}

	public boolean immediatelyAddBackupSignal(Address trueHolder, Signal signal) {
		return backups.put(trueHolder, signal);
	}

	protected boolean immediatelyAddBackupSignal(Address trueHolder,
			Collection<Signal> signal) {
		return backups.putAll(trueHolder, signal);
	}

	/**
	 * Returns the signals backed up for addr but that are not contained in
	 * doNotContain
	 * */
	public Collection<Signal> getBackupsForAndRemove(Address addr,
			List<Signal> doNotContain) {
		Collection<Signal> list = backups.get(addr);
		list.removeAll(doNotContain);
		return Lists.newArrayList(list);
	}

	public synchronized void changeBackups(Address oldOwner, Address newOwner,
			Collection<Signal> signals) {
		Collection<Signal> oldBackup = backups.get(oldOwner);
		if (oldBackup.removeAll(signals)) {
			Collection<Signal> newBackup = backups.get(newOwner);
			newBackup.addAll(signals);
		}
	}
	
	public void addBackupSignals(Address holder, Collection<Signal> signal) {
		backups.get(holder).addAll(signal);
	}

	public void removeBackupFor(Address addr) {
		backups.removeAll(addr);
	}

	public void removeTheseSignalsFromBackup(Address addr, List<Signal> toDelete) {
		backups.get(addr).removeAll(toDelete);
	}

	@Override
	public Result findSimilarTo(Signal signal) throws RemoteException {
		if (!acceptRequests.get()) {
			return null;
		}
		long id = requestId.incrementAndGet();
		externalRelations.process(id, signal);
		// wait till system is recovered
		Result result = processSignal(signal);
		ResultContainer results = externalRelations.get(id);
		if (results.getRetry()) {
			result = processSignal(signal);
		}
		List<Result> list = Lists.newArrayList();
		list.add(result);
		list.addAll(results.getResults());
		return mergeResults(list);
	}

	public Result processSignal(Signal signal) {
		try {
			List<Result> results = Lists.newArrayList();
			List<Callable<Result>> callables = Lists.newArrayList();
			LinkedBlockingQueue<Signal> copy = null;
			synchronized (mySignals) {
				copy = new LinkedBlockingQueue<>(mySignals);
			}
			synchronized (newSignals) {
				copy.addAll(new LinkedBlockingQueue<>(newSignals));
			}
			for (int i = 0; i < nThreads; i++) {
				callables.add(new SignalProcessorThread(signal, copy));
			}
			List<Future<Result>> futureResults = mainThread
					.invokeAll(callables);
			for (Future<Result> future : futureResults) {
				results.add(future.get());
			}
			return mergeResults(results);
		} catch (InterruptedException | ExecutionException e) {
			throw new InternalError();
		}
	}

	private Result mergeResults(List<Result> results) {
		Result result = results.get(0);
		for (int i = 0; i < results.size(); i++) {
			Result inner = results.get(i);
			for (Item item : inner.items()) {
				result = result.include(item);
			}
		}

		return result;
	}

	public List<Signal> getBackedUpSignals(Address addr) {
		return Lists.newArrayList(backups.get(addr));
	}

	/**
	 * ***************SPNODE IMPLEMENTATION******************
	 * */

	@Override
	public synchronized void join(String clusterName) throws RemoteException {
		if (!mySignals.isEmpty() || !newSignals.isEmpty()) {
			throw new IllegalStateException("Can't join a cluster because there are signals already stored");
		}
		if (started.get()) {
			return;
		}
		externalRelations.join(clusterName);
		externalRelationsThread.start();
		externalRelations.readyToPlay();
		started.set(true);
	}

	public BlockingQueue<Signal> getSignals() {
		synchronized (mySignals) {
			return new LinkedBlockingQueue<>(mySignals);
		}
	}
	
	public void removeSignals(Collection<Signal> signals) {
		synchronized (mySignals) {
			mySignals.removeAll(signals);
		}
	}

	@Override
	public synchronized void exit() throws RemoteException {
		mySignals.clear();
		backups.clear();
		newSignals.clear();
		if (!started.get()) {
			return;
		}
		acceptRequests.set(false);
		externalRelationsThread.interrupt();
		try {
			externalRelationsThread.join();
		} catch (InterruptedException e1) {
		}
		externalRelations.removeMe();
		externalRelations.readyToExit();
		mainThread.shutdown();
		try {
			while (!mainThread.awaitTermination(1000, TimeUnit.MILLISECONDS))
				;
			this.mainThread = Executors.newFixedThreadPool(nThreads);
			externalRelations = new ExternalRelations(this);
			externalRelationsThread = new Thread(externalRelations);
		} catch (InterruptedException e) {
		} catch (Exception e) {
		} finally {			
			started.set(false);
		}
	}

	@Override
	public NodeStats getStats() throws RemoteException {
		return new NodeStats(externalRelations.getAddressAsString(),
				requests.get(), mySignals.size(), backups.size(),
				externalRelations.isDegraded());
	}

	public long backupSize() {
		return backups.size();
	}

	public long signalSize() {
		return mySignals.size();
	}

}
