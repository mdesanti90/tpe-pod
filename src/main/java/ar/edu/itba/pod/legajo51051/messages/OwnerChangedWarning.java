package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;
import java.util.List;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.legajo51051.impl.AddressSignal;

public class OwnerChangedWarning implements Serializable {

	private final List<Signal> sent;
	private final Address newOwner;
	private final Address oldOwner;

	public OwnerChangedWarning(List<Signal> sent, Address newOwner, Address oldOwner) {
		this.sent = sent;
		this.newOwner = newOwner;
		this.oldOwner = oldOwner;
	}

	public List<Signal> getSent() {
		return sent;
	}

	public Address getNewOwner() {
		return newOwner;
	}
	
	public Address getOldOwner() {
		return oldOwner;
	}

}
