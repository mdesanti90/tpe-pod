package ar.edu.itba.pod.legajo51051.impl.notifications;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import org.jgroups.Address;
import org.jgroups.JChannel;

import ar.edu.itba.pod.api.Signal;
import ar.edu.itba.pod.legajo51051.impl.MultiThreadedSignalProcessor;
import ar.edu.itba.pod.legajo51051.messages.AddBackupSignals;
import ar.edu.itba.pod.legajo51051.messages.InitialSyncDone;
import ar.edu.itba.pod.legajo51051.messages.LoadBalancingMessage;
import ar.edu.itba.pod.legajo51051.messages.OwnerChangedWarning;

import com.google.common.collect.Lists;

public class NewMemberNotification implements PriorityNotification {

	private final Address newMember;
	private final JChannel channel;
	private final BlockingQueue<Address> members;
	private final MultiThreadedSignalProcessor processor;
	private final Logger log = Logger.getLogger("Notifications: new member");
	private final AtomicBoolean isDegraded;
	private final static int CHUNK_SIZE = 500;
	private final Semaphore newNodeSync;

	public NewMemberNotification(final MultiThreadedSignalProcessor processor,
			final JChannel channel, final BlockingQueue<Address> members,
			final Address newMember, final AtomicBoolean isDegraded,
			final Semaphore newNodeSync) {
		this.newMember = newMember;
		this.channel = channel;
		this.members = members;
		this.processor = processor;
		this.isDegraded = isDegraded;
		this.newNodeSync = newNodeSync;
	}

	public Address getNewMember() {
		return newMember;
	}

	@Override
	public NotificationPriority getPriority() {
		return NotificationPriority.NORMAL;
	}

	@Override
	public void process() {
		this.isDegraded.set(true);
		log.info(getNewMember() + " has joined");
		members.add(getNewMember());
		int size = members.size();
		log.info("Selecting signals to send ");
		BlockingQueue<Signal> signals = new LinkedBlockingQueue<>(
				processor.getSignals());
		log.info("Signals selected");
		int loops = (signals.size() / members.size()) / CHUNK_SIZE;
		try {
			log.info("AAA");
			for (int i = 0; i < loops; i++) {
				List<Signal> toSend = Lists.newArrayList();
				signals.drainTo(toSend, CHUNK_SIZE);
				processor.removeSignals(toSend);
				if (size == 2) {
					processor.addBackupSignals(getNewMember(), toSend);
				}
				channel.send(getNewMember(), new LoadBalancingMessage(toSend));
				channel.send(null, new OwnerChangedWarning(toSend,
						getNewMember(), channel.getAddress()));
			}
			log.info("sent messages");
			if (size == 2) {
				log.info("Backups selected - Sending...");
				while (!signals.isEmpty()) {
					Collection<Signal> list = Lists.newArrayList();
					signals.drainTo(list, CHUNK_SIZE);
					channel.send(getNewMember(), new AddBackupSignals(list,
							channel.getAddress()));
				}
				log.info("Backups sent - changing owner...");
				processor.removeBackupFor(channel.getAddress());
			}
			log.info("sending sync msg");
			List<Address> addr = Lists.newArrayList(members);
			for (int i = 0; i < members.size(); i++) {
				channel.send(addr.get(i), new InitialSyncDone(getNewMember()));
			}
			log.info("InitialSyncSent " + members.size());
			while (!newNodeSync.tryAcquire(members.size() - 2, 1000,
					TimeUnit.MILLISECONDS))
				;
			log.info("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.isDegraded.set(false);
	}

}
