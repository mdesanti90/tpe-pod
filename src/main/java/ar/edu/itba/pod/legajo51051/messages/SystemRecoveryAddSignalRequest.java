package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;
import java.util.List;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Signal;

public class SystemRecoveryAddSignalRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final boolean isBackup;
	private final List<Signal> signal;
	private final Address backupHolder;
	private final long id;
	
	public SystemRecoveryAddSignalRequest(Address trueSignalCarrier, List<Signal> signal, boolean isBackup, long id) {
		this.isBackup = isBackup;
		this.signal = signal;
		this.backupHolder = trueSignalCarrier;
		this.id = id;
	}
	
	public List<Signal> getSignal() {
		return signal;
	}
	
	public boolean isBackup() {
		return isBackup;
	}
	
	public Address getBackupHolder() {
		return backupHolder;
	}
	
	public long getId() {
		return id;
	}

}
