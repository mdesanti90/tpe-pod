package ar.edu.itba.pod.legajo51051.messages;

import java.io.Serializable;

public class SystemRecoveryAddBackupSignalACK implements Serializable {

	private long id;
	
	public SystemRecoveryAddBackupSignalACK(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
}
