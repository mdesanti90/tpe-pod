package ar.edu.itba.pod.legajo51051.impl;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import org.jgroups.Address;

import ar.edu.itba.pod.api.Result;
import ar.edu.itba.pod.api.Signal;

public class PendingProcess {

	private final long processId;
	private final BlockingQueue<Address> members;
	private final AtomicInteger membersQty;
	private final Semaphore sem;
	private final BlockingQueue<Result> results = new LinkedBlockingQueue<>();
	private final Logger log = Logger.getLogger("Pending Process");
	private final Signal beingProcessed;
	private boolean retry = false;
	private long timeStamp;
	
	public PendingProcess(long timeStamp, Signal beingProcessed, long processId, Collection<Address> members) {
		this.processId = processId;
		this.members = new LinkedBlockingQueue<>(members);
		this.sem = new Semaphore(0);
		this.membersQty = new AtomicInteger(members.size());
		this.beingProcessed = beingProcessed;
		this.timeStamp = timeStamp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (processId ^ (processId >>> 32));
		return result;
	}
	
	public synchronized void newResult(Address from, Result result) {
		members.remove(from);
		results.add(result);
		sem.release();
	}
	
	public List<Result> get() {
		try {
			while(!sem.tryAcquire(membersQty.get()-1, 1000, TimeUnit.MILLISECONDS)){
				log.info("Request " + processId + " still waiting");
			}
			return new LinkedList<>(results);
		} catch (InterruptedException e) {
		}
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PendingProcess other = (PendingProcess) obj;
		if (processId != other.processId)
			return false;
		return true;
	}
	
	public void nodeHasFallen(List<Address> newMembers) {
		sem.drainPermits();
		results.clear();
		members.clear();
		members.addAll(newMembers);
		membersQty.set(newMembers.size());
		retry = true;
	}
	
	public BlockingQueue<Address> getMembers() {
		return members;
	}
	
	public Signal getBeingProcessed() {
		return beingProcessed;
	}
	
	public long getProcessId() {
		return processId;
	}
	
	public boolean isRetry() {
		return retry;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

}
