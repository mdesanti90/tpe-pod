package ar.edu.itba.pod.legajo51051.impl.notifications;

public interface PriorityNotification {

	public NotificationPriority getPriority();
	
	public void process();
	
}
